# HELP WANTED

Version: 0.0.2

This paper is a work in progress and we are looking for contributors.

# Introduction

The goal of this paper is to solidify the idea of annotations and container behavior of HPC containers presented at FOSDEM 2023 (among other occasions) cite[@fosdem_2023_hpcc].

# Expected Container Behavior

HPC containers are expected to provide a similar experience as a loging into a shell on a node.
This means that a container ultimately drops into a shell (`bash`, `sh`) with a minimal `ENTRYPOINT` within the container.

The reason for this is that the run command of a container is expected to be dropped into a run script on the host.

```bash
mpirun gmx_mpi mdrun -s ${INPUT} -resethway
```

To

```bash
mpirun sarus run --mpi \
  quay.io/gromacs/2021.5/mpich:x86_64_v4 \ 
  gmx_mpi mdrun -s ${INPUT} -resethway
```

# Annotations

Annotations are a way to describe the container in a machine readable way.

# Appendix

## Appendix A: Annotation Groups

### `org.supercontainers.hardware`

This group contains all annotations that describe for which hardware the libraries and binaries are compiled.

| Suffix                    | Example                        |
| ------------------------- | ------------------------------ |
| `cpu.target`              | generic / microarch            |
| `cpu.target.generic`      | x86_64_v4                      |
| `cpu.target.microarch`    | skylake / skylake_avx512       |
| `gpu.vendor`              | nvidia/amd/intel               |
| `gpu.nvidia.cuda.version` | 12.1                           |
| `gpu.nvidia.architecture` | sm_35 (kepler)/ sm_86 (ampere) |
