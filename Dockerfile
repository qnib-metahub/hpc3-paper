ARG BASE_IMAGE=ubuntu:18.04
FROM ${BASE_IMAGE}
ENV DEBIAN_FRONTEND=noninteractive
RUN <<EOF
  apt update
  apt install -y make \
    pandoc \
    pandoc-citeproc \
    texlive-full
EOF
